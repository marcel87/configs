" global indent config:
"   * set every indent to two spaces
set expandtab
set smarttab
set tabstop=2
set softtabstop=2
set shiftwidth=2

" per file type indentation
" PYTHON
autocmd Filetype python setlocal ts=4 sw=4 expandtab
" JAVASCRIPT
autocmd Filetype javascript setlocal ts=4 sw=4 expandtab

" Show relative line numbers
set number
set relativenumber
